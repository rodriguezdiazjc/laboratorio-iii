/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class BaseDatos {
    
    private Connection connection = null;
    private ResultSet rs = null;
    private Statement s = null;
    
    public BaseDatos() {
        this.Conexion();
    }
    
    public void Conexion() {
        if (connection != null) {
            return;
        }
        String url = "jdbc:postgresql://localhost:5432/datos_l3";
        String password = "JeanCa123.";
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, "postgres", password);
            if (connection != null) {
                System.out.println("Connecting to database...");
            }
        } catch (Exception e) {
            System.out.println("Problem when connecting to the database");
        }
    }
    
    public boolean agregarJugador(int pCedula, String pNombre, int pEdad, boolean pEstado, int pIdEquipo, int pIdDestreza, Date pFechaDebut) {
        boolean registroExitoso = false;
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("INSERT INTO jugadores (cedula, nombre, edad, estado, equipo, destreza, fecha_debut)"
                    + "VALUES ('" + pCedula + "', '" + pNombre + "', '" + pEdad + "', '"
                    + pEstado + "', '" + pIdEquipo + "', '" + pIdDestreza + "', '" + pFechaDebut + "')");
            if (z == 1) {
                registroExitoso = true;
            } else {
                registroExitoso = false;
            }
        } catch (Exception e) {
            registroExitoso = false;
        }
        return registroExitoso;
    }
    
    public ArrayList<Integer> consultaID() {
        ArrayList<Integer> consulta = new ArrayList<Integer>();
        boolean datosEncontrados = false;
        try {
            s = connection.createStatement();
            rs = s.executeQuery("SELECT cedula FROM jugadores");
            while (rs.next()) {
                consulta.add(Integer.parseInt(rs.getString("cedula")));
                datosEncontrados = true;
            }
        } catch (Exception e) {
            datosEncontrados = false;
        }
        if (!datosEncontrados) {
            consulta = null;
        }
        return consulta;
    }
    
    public ArrayList<String[]> consultaVelocidad() {
        ArrayList<String[]> consulta = new ArrayList<String[]>(); 
        boolean datosEncontrados = false;
        try {
            s = connection.createStatement();
            rs = s.executeQuery("SELECT j.cedula, j.nombre, e.nombre_equipo, d.perfil FROM jugadores j, equipo e, destrezas d WHERE j.destreza = d.id_destreza and j.equipo = e.id_equipo and d.fortaleza = 'velocidad' and j.edad > 20");
            while (rs.next()) {
                String[] fila = new String[4];
                fila[0] = rs.getString("cedula");
                fila[1] = rs.getString("nombre");
                fila[2] = rs.getString("nombre_equipo");
                fila[3] = rs.getString("perfil");
                consulta.add(fila);
                datosEncontrados = true;
            }
        } catch (Exception e) {
            datosEncontrados = false;
        }
        if (!datosEncontrados) {
            consulta = null;
        }
        return consulta;
    }
    
    public ArrayList<String[]> consultaFechaDebut(Date pFechaInicial, Date pFechaFinal) {
        ArrayList<String[]> consulta = new ArrayList<String[]>();
        boolean datosEncontrados = false;
        try {
            s = connection.createStatement();
            rs = s.executeQuery("SELECT j.nombre, e.nombre_equipo, j.fecha_debut FROM jugadores j, equipo e WHERE j.equipo = e.id_equipo AND (j.fecha_debut BETWEEN '"+ pFechaInicial +"' AND '"+ pFechaFinal +"')");
            while (rs.next()) {
                String[] fila = new String[3];
                fila[0] = rs.getString("nombre");
                fila[1] = rs.getString("nombre_equipo");
                fila[2] = rs.getString("fecha_debut");
                consulta.add(fila);
                datosEncontrados = true;
            }
        } catch (Exception e) {
            datosEncontrados = false;
        }
        if (!datosEncontrados) {
            consulta = null;
        }
        return consulta;
    }
    
    public ArrayList<String[]> consultaEdad(int pEdadInicial, int pEdadFinal) {
        ArrayList<String[]> consulta = new ArrayList<String[]>();
        boolean datosEncontrados = false;
        try {
            s = connection.createStatement();
            rs = s.executeQuery("SELECT j.cedula, j.nombre, j.edad, j.fecha_debut FROM jugadores j, equipo e, destrezas d WHERE (j.equipo = e.id_equipo) AND (j.estado = true) AND (j.fecha_debut > '1997/01/01') AND (e.estado = true) AND (j.destreza = d.id_destreza) AND (d.fortaleza = 'cabeceo') AND (j.edad BETWEEN '"+ pEdadInicial +"' AND '"+ pEdadFinal +"')");
            while (rs.next()) {
                String[] fila = new String[4];
                fila[0] = rs.getString("cedula");
                fila[1] = rs.getString("nombre");
                fila[2] = rs.getString("edad");
                fila[3] = rs.getString("fecha_debut");
                consulta.add(fila);
                datosEncontrados = true;
            }
        } catch (Exception e) {
            datosEncontrados = false;
        }
        if (!datosEncontrados) {
            consulta = null;
        }
        return consulta;
    }

    public ArrayList<String[]> equipos() {
        ArrayList<String[]> listaEquipos = new ArrayList<String[]>();
        try {
            s = connection.createStatement();
            rs = s.executeQuery("SELECT id_equipo, nombre_equipo FROM equipo");
            while (rs.next()) {
                String[] fila = new String[2];
                fila[0] = rs.getString("id_equipo");
                fila[1] = rs.getString("nombre_equipo");
                listaEquipos.add(fila);
            }
        } catch (Exception e) {
            listaEquipos = null;
        }
        return listaEquipos;
    }
    
}
