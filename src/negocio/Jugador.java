/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import datos.BaseDatos;
import java.sql.Date;

/**
 *
 * @author User
 */
public class Jugador {
    
    public boolean validarID(int pId) {
        boolean valido = false;
        BaseDatos oBD = new BaseDatos();
        if (oBD.consultaID() != null) {
            if (!oBD.consultaID().contains(pId)) {
                valido = true;
            }
        } else {
            valido = true;
        }
        return valido;
    }
    
    public boolean validarCedulaYEdad (String pValor) {
        boolean valida = true;
        try {
            int edad = Integer.parseInt(pValor);
        } catch (Exception e) {
            valida = false;
        }
        return valida;
    }
    
    public int idDestreza(String pPerfil, String pDestreza) {
        int id = -1;
        if (pPerfil.equals("Derecho")) {
            switch (pDestreza) {
                case "Cabeceo":
                    id = 1;
                    break;
                case "Velocidad":
                    id = 2;
                    break;
                case "Marca":
                    id = 3;
                    break;
            }
        } else {
            switch (pDestreza) {
                case "Cabeceo":
                    id = 4;
                    break;
                case "Velocidad":
                    id = 5;
                    break;
                case "Marca":
                    id = 6;
                    break;
            }
        }
        return id;
    }
    
}
