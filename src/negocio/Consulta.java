/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import datos.BaseDatos;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author User
 */
public class Consulta {

    BaseDatos oBD;

    public Consulta() {
        this.oBD = new BaseDatos();
    }

    public DefaultTableModel modeloTabla = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };

    public DefaultTableModel getModeloTabla() {
        return modeloTabla;
    }

    public boolean cargarConsultaVelocidad() {
        boolean valorRetorno = false;
        ArrayList<String[]> consulta = this.oBD.consultaVelocidad();
        if (consulta != null) {
            this.modeloTabla = new DefaultTableModel();
            this.modeloTabla.addColumn("Cédula");
            this.modeloTabla.addColumn("Nombre");
            this.modeloTabla.addColumn("Equipo");
            this.modeloTabla.addColumn("Perfil");
            for (int i = 0; i < consulta.size(); i++) {
                this.modeloTabla.addRow(consulta.get(i));
            }
            valorRetorno = true;
        }
        return valorRetorno;
    }

    public boolean cargarConsultaFechaDebut(Date pFechaInicial, Date pFechaFinal) {
        boolean valorRetorno = false;
        ArrayList<String[]> consulta = this.oBD.consultaFechaDebut(pFechaInicial, pFechaFinal);
        if (consulta != null) {
            this.modeloTabla = new DefaultTableModel();
            this.modeloTabla.addColumn("Nombre");
            this.modeloTabla.addColumn("Equipo");
            this.modeloTabla.addColumn("Fecha debut");
            for (int i = 0; i < consulta.size(); i++) {
                this.modeloTabla.addRow(consulta.get(i));
            }
            valorRetorno = true;
        }
        return valorRetorno;
    }

    public boolean cargarConsultaEdad(int pEdadInicial, int pEdadFinal) {
        boolean valorRetorno = false;
        ArrayList<String[]> consulta = this.oBD.consultaEdad(pEdadInicial, pEdadFinal);
        if (consulta != null) {
            this.modeloTabla = new DefaultTableModel();
            this.modeloTabla.addColumn("Cédula");
            this.modeloTabla.addColumn("Nombre");
            this.modeloTabla.addColumn("Edad");
            this.modeloTabla.addColumn("Fecha debut");
            Collections.sort(consulta, new Comparator<String[]>() {
                public int compare(String[] s1, String[] s2) {
                    return s1[1].compareTo(s2[1]);
                }
            });
            for (int i = 0; i < consulta.size(); i++) {
                this.modeloTabla.addRow(consulta.get(i));
            }
            valorRetorno = true;
        }
        return valorRetorno;
    }
    
}
